# README #

This project runs sample auto-tests for Gmail only.

### Running ###

```
./gradlew test
```

### Configuration ###

Configuration parameters:

```
browserType=CHROME // Possible values are CHROME and FIREFOX
websiteUrl=https://mail.google.com // URL for accessing Gmail
login=user@gmail.com
password=foo
```

Test run configuration is loaded from `config.base.properties` file. You can either edit it directly or make another file `config.personal.properties`, which would override any entries of base config (handy for `login` and `password`).