package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import selenium.Button;
import selenium.TextField;
import selenium.WebElementContainer;

public class SearchContainer extends WebElementContainer {
    public SearchContainer(WebElement webElement, WebDriver webDriver) {
        super(webElement, webDriver);
    }

    @FindBy(css = "input[type=\"text\"]")
    private TextField searchField;

    @FindBy(tagName = "button")
    private Button searchButton;

    public void searchEmail(String searchText){
        searchField.fill(searchText);
        searchButton.clickButton();
        waitForLoad();
    }
}
