package page;

import model.EmailInfo;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import selenium.Button;
import selenium.Link;
import selenium.Row;

import java.util.ArrayList;
import java.util.List;

public class MailPage extends Page {
    public MailPage(WebDriver webDriver) {
        super(webDriver);
    }

    // Here and in other places are used not readable selectors of because Google politic
    @FindBy(css = ".TK a")
    private List<Link> foldersList;

    @FindBy(css = ".BltHke[role=\"main\"] .xY [role=\"link\"]")
    private List<Row> emailsList;

    @FindBy(css = "[role=\"search\"] form")
    private SearchContainer searchContainer;

    @FindBy(css = "#gb .gb_mb a")
    private Button accountInfoButton;

    private @FindBy(css = ".gb_jb .gb_xb")
    AccountInfoContainer accountInfoContainer;

    @FindBy(css = ".Bs[role='presentation']")
    private EmailContentContainer emailContentContainer;

    @FindBy(css = ".G-tF .lS[role='button']")
    private Button backToSearchResultButton;

    public void selectMailFolder(String folderName) {
        // I use here href attribute to select folder because
        // I can't control user's browser localization to use folder title
        for (Link folder : foldersList) {
            if (folder.getAttribute("href").contains(folderName)) {
                boolean isOpened = folder.getAttribute("tabindex").contains("0");
                folder.clickLink();
                if (!isOpened)
                    waitForDataReload();
                break;
            }
        }
    }

    public List<Row> getEmailsListByText(String text) {
        searchContainer.searchEmail(text);
        waitForDataReload();
        return emailsList;
    }

    public ArrayList<EmailInfo> getEmailsInfoByText(String text) {
        ArrayList<EmailInfo> emailsInfoList = new ArrayList<>();
        List<Row> foundEmails = getEmailsListByText(text);

        for (Row email : foundEmails) {
            waitForElementVisible(email);
            email.selectRow();
            emailsInfoList.add(emailContentContainer.getEmailInfo());
            backToSearchResult();
        }
        return emailsInfoList;
    }

    public void backToSearchResult() {
        waitForElementClickable(backToSearchResultButton);
        backToSearchResultButton.click();
    }

    public void logout() {
        accountInfoButton.clickButton();
        accountInfoContainer.logout();
    }
}
