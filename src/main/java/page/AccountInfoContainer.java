package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import selenium.Button;
import selenium.WebElementContainer;

public class AccountInfoContainer extends WebElementContainer {
    public AccountInfoContainer(WebElement webElement, WebDriver webDriver) {
        super(webElement, webDriver);
    }

    @FindBy(id = "gb_71")
    private Button logoutButton;

    public void logout(){
        waitForElementClickable(logoutButton);
        logoutButton.click();
        waitForLoad();
    }
}
