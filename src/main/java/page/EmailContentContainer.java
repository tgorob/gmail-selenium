package page;

import model.EmailInfo;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import selenium.Label;
import selenium.WebElementContainer;

public class EmailContentContainer extends WebElementContainer {
    public EmailContentContainer(WebElement webElement, WebDriver webDriver) {
        super(webElement, webDriver);
    }

    @FindBy(css = ".ha h2")
    private Label subjectLabel;

    @FindBy(css = ".gF span")
    private Label senderLabel;

    @FindBy(className = "ii")
    private Label messageLabel;

    public EmailInfo getEmailInfo() {
        waitForElementVisible(senderLabel);
        return new EmailInfo(senderLabel.getLabelText(), null, subjectLabel.getLabelText(), messageLabel.getLabelText());
    }
}
