package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import selenium.Button;
import selenium.TextField;

public class LoginPage extends Page {
    public LoginPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(id = "identifierId")
    private TextField emailField;

    @FindBy(css = "#password input")
    private TextField passwordField;

    @FindBy(id = "identifierNext")
    private Button loginNextButton;

    @FindBy(id = "passwordNext")
    private Button passwordNextButton;

    public void login(String email, String password) {
        emailField.fill(email);
        loginNextButton.clickButton();
        waitForElementVisible(passwordField);
        passwordField.fill(password);
        passwordNextButton.clickButton();
        waitForLoad();

    }
}
