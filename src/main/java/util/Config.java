package util;

import selenium.BrowserType;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {
    private Properties properties;

    public static Config load() throws IOException {
        Properties properties = new Properties();

        File baseConfig = new File("./config.base.properties");
        try (InputStream input = new FileInputStream(baseConfig)) {
            properties.load(input);
        }

        File personalConfig = new File("./config.personal.properties");
        if (personalConfig.exists()) {
            try (InputStream input = new FileInputStream(personalConfig)) {
                properties.load(input);
            }
        }

        return new Config(properties);
    }

    public Config(Properties properties) {
        this.properties = properties;
    }

    public BrowserType getBrowserType() {
        return BrowserType.valueOf(properties.getProperty("browserType"));
    }

    public String getWebsiteUrl() {
        return properties.getProperty("websiteUrl");
    }

    public String getLogin() {
        return properties.getProperty("login");
    }

    public String getPassword() {
        return properties.getProperty("password");
    }
}
