package util;

import model.EmailInfo;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

public class Email {

    public static void send(EmailInfo emailInfo, String password) throws MessagingException {
        //TODO properties should be moved to config file
        Properties props = System.getProperties();
        props.setProperty("mail.smtp.host", "smtp.gmail.com");
        props.setProperty("mail.user", emailInfo.getFromName());
        props.setProperty("mail.password", password);
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");
        props.setProperty("mail.smtp.ssl.enable", "true");
        props.setProperty("mail.store.protocol", "imaps");
        Session session = Session.getInstance(props);

        try {
            MimeMessage mimeMessage = new MimeMessage(session);
            mimeMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(emailInfo.getToName()));
            mimeMessage.setSubject(emailInfo.getSubject());
            mimeMessage.setFrom(new InternetAddress(emailInfo.getFromName()));

            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setText(emailInfo.getMessage());
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);
            mimeMessage.setContent(multipart);

            Transport transport = session.getTransport("smtp");
            transport.connect("smtp.gmail.com", emailInfo.getFromName(), password);
            transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
        } catch (Exception e) {
            throw new MessagingException("Sending failed", e);
        }
    }
}
