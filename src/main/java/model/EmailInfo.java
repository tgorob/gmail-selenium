package model;

public class EmailInfo {
    private String fromName;
    private String toName;
    private String subject;
    private String message;

    public EmailInfo(String fromEmailAddress, String toEmailAddress, String subject, String message) {
        this.fromName = fromEmailAddress;
        this.toName = toEmailAddress;
        this.subject = subject;
        this.message = message;
    }

    public String getFromName() {
        return fromName;
    }

    public String getToName() {
        return toName;
    }

    public String getSubject() {
        return subject;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EmailInfo emailInfo = (EmailInfo) o;

        if (fromName != null ? !fromName.equals(emailInfo.fromName) : emailInfo.fromName != null) return false;
        if (subject != null ? !subject.equals(emailInfo.subject) : emailInfo.subject != null) return false;
        return message != null ? message.equals(emailInfo.message) : emailInfo.message == null;
    }

    @Override
    public int hashCode() {
        int result = fromName != null ? fromName.hashCode() : 0;
        result = 31 * result + (toName != null ? toName.hashCode() : 0);
        result = 31 * result + (subject != null ? subject.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        return result;
    }
}
