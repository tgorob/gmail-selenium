package selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Row extends WebElementWrapper {
    public Row(WebElement webElement, WebDriver webDriver) {
        super(webElement, webDriver);
    }

    public void selectRow() {
        this.click();
    }
}
