package selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Label extends WebElementWrapper {
    public Label(WebElement webElement, WebDriver webDriver) {
        super(webElement, webDriver);
    }

    public String getLabelText() {
        return this.getText().replaceAll("[\\r\\n]", " ");
    }
}
