package selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Link extends WebElementWrapper {
    public Link(WebElement webElement, WebDriver webDriver) {
        super(webElement, webDriver);
    }

    public void clickLink() {
        this.clickJS();
    }
}
