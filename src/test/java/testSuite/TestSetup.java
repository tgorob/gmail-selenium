package testSuite;

import model.EmailInfo;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import page.LoginPage;
import selenium.Browser;
import util.Config;
import util.Email;

public class TestSetup {
    protected WebDriver webDriver;
    protected EmailInfo expectedEmailInfo;

    @Before
    public void setUp() throws Exception {
        Config config = Config.load();

        expectedEmailInfo = new EmailInfo(config.getLogin(), config.getLogin(), TestDataGenerator.nextUniqueSubject(),
                TestDataGenerator.nextUniqueMessage());
        Email.send(expectedEmailInfo, config.getPassword());
        webDriver = new Browser(config).getWebDriver();
        login(config.getLogin(), config.getPassword());
    }

    @After
    public void after() {
        webDriver.quit();
    }

    private void login(String login, String password){
        LoginPage loginPage = new LoginPage(webDriver);
        loginPage.init();
        loginPage.login(login, password);
    }
}
