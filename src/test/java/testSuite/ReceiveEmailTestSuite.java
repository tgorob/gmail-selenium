package testSuite;

import model.EmailInfo;
import model.MailFolders;
import org.junit.Assert;
import org.junit.Test;
import page.MailPage;

import java.util.ArrayList;

public class ReceiveEmailTestSuite extends TestSetup {
    @Test
    public void checkSentEmailWasReceivedWithExpectedContent() {
        MailPage mailPage = new MailPage(webDriver);
        mailPage.init();
        mailPage.waitForLoad();

        mailPage.selectMailFolder(MailFolders.INBOX);
        ArrayList<EmailInfo> emailsInfo = mailPage.getEmailsInfoByText(expectedEmailInfo.getSubject());

        mailPage.logout();

        Assert.assertTrue(emailsInfo.size() == 1);
        Assert.assertEquals(expectedEmailInfo, emailsInfo.get(0));
    }
}
