package testSuite;

import java.util.UUID;

public class TestDataGenerator {
    public static String nextUniqueSubject() {
        return String.format("Subject %s", UUID.randomUUID());
    }

    public static String nextUniqueMessage() {
        return String.format("Message %s", UUID.randomUUID());
    }
}
